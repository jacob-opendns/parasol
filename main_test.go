package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestUrlToCheck(t *testing.T) {
	assertUrlToCheckResult(t, "/urlinfo/1/", "")
	assertUrlToCheckResult(t, "/urlinfo/1/evil.com", "evil.com")
	assertUrlToCheckResult(t, "/urlinfo/1/evil.com:666", "evil.com:666")
	assertUrlToCheckResult(t, "/urlinfo/1/evil.com:666/foo", "evil.com:666/foo")
	assertUrlToCheckResult(t, "/urlinfo/1/evil.com:666/foo?bar=baz", "evil.com:666/foo?bar=baz")
}

func assertUrlToCheckResult(t *testing.T, arg string, expected string) {
	got := urlToCheck(arg)

	if got != expected {
		t.Errorf("Got %v from urlToCheck for \"%v\". Should've gotten %v", got, arg, expected)
	}
}

func TestUrlinfoWithBlacklistedURL(t *testing.T) {
	assertUrlinfoResponse(t, "/urlinfo/1/evil.com", http.StatusOK)
	assertUrlinfoResponse(t, "/urlinfo/1/evil.com/wormz", http.StatusOK)
	assertUrlinfoResponse(t, "/urlinfo/1/evil.com/warez", http.StatusOK)
	assertUrlinfoResponse(t, "/urlinfo/1/badguyz.com:666/foo", http.StatusOK)
	assertUrlinfoResponse(t, "/urlinfo/1/badguyz.com:666/bar?baz=quux", http.StatusOK)
}

func TestUrlinfoWithUnkownURL(t *testing.T) {
	assertUrlinfoResponse(t, "/urlinfo/1/abc", http.StatusNotFound)
	assertUrlinfoResponse(t, "/urlinfo/1/abc/def", http.StatusNotFound)
	assertUrlinfoResponse(t, "/urlinfo/1/abc/def/ghi", http.StatusNotFound)
	assertUrlinfoResponse(t, "/urlinfo/1/abc/def/ghi?jkl=mno", http.StatusNotFound)
	assertUrlinfoResponse(t, "/urlinfo/1/badguyz.com:666/bar?baz=thud", http.StatusNotFound)
}

func assertUrlinfoResponse(t *testing.T, url string, expectedStatus int) {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()
	u := urlInfoHandler{blacklistChecker: &trivialBlacklistChecker{}}
	handler := http.HandlerFunc(u.urlinfo)

	handler.ServeHTTP(responseRecorder, request)

	if status := responseRecorder.Code; status != expectedStatus {
		t.Errorf("wrong status code for \"%v\": %v (%v)", url, status, expectedStatus)
	}

	expectedBodyLength := 0
	if bodyLength := len(responseRecorder.Body.String()); bodyLength != expectedBodyLength {
		t.Errorf("unexpected body length: %v (%v)", bodyLength, expectedBodyLength)
	}
}

func TestUrlinfoReturns405IfMethodNotGET(t *testing.T) {
	assertUrlinfoReturns405IfMethodNotGET(t, "POST")
	assertUrlinfoReturns405IfMethodNotGET(t, "PUT")
	assertUrlinfoReturns405IfMethodNotGET(t, "DELETE")
	assertUrlinfoReturns405IfMethodNotGET(t, "OPTIONS")
	assertUrlinfoReturns405IfMethodNotGET(t, "TRACE")
	assertUrlinfoReturns405IfMethodNotGET(t, "CONNECT")
}

func assertUrlinfoReturns405IfMethodNotGET(t *testing.T, method string) {
	request, err := http.NewRequest(method, "/urlinfo/1/badguyz.com:666/bar?baz=thud", nil)
	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()
	u := urlInfoHandler{blacklistChecker: nil}
	handler := http.HandlerFunc(u.urlinfo)

	handler.ServeHTTP(responseRecorder, request)

	expectedStatus := http.StatusMethodNotAllowed
	if status := responseRecorder.Code; status != expectedStatus {
		t.Errorf("wrong status code for %v: %v (%v)", method, status, expectedStatus)
	}
}

type trivialBlacklistChecker struct {
}

func (t trivialBlacklistChecker) check(url string) (error, bool) {
	var blacklist = map[string]struct{}{"evil.com": {},
		"evil.com/wormz":               {},
		"evil.com/warez":               {},
		"badguyz.com:666/foo":          {},
		"badguyz.com:666/foo?bar=baz":  {},
		"badguyz.com:666/bar?baz=quux": {}}
	_, blacklisted := blacklist[url]
	return nil, blacklisted
}
