package main

import (
	"github.com/garyburd/redigo/redis"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	handler := urlInfoHandler{blacklistChecker: &myBlacklistChecker{}}

	http.HandleFunc("/urlinfo/1/", handler.urlinfo)

	port := os.Getenv("PORT")

	log.Println("listening on", port, "...")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

type urlInfoHandler struct {
	blacklistChecker BlacklistChecker
}

func (s *urlInfoHandler) urlinfo(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET":
		url := urlToCheck(req.URL.String())
		log.Println("handling urlinfo request for", url)

		if err, blacklisted := s.blacklistChecker.check(url); err != nil {
			res.WriteHeader(http.StatusInternalServerError)
		} else if blacklisted {
			res.WriteHeader(http.StatusOK)
		} else {
			res.WriteHeader(http.StatusNotFound)
		}
	default:
		res.WriteHeader(http.StatusMethodNotAllowed)
	}
}

type BlacklistChecker interface {
	check(url string) (error, bool)
}

type myBlacklistChecker struct {
}

func (t myBlacklistChecker) check(url string) (error, bool) {
	c, err := redis.Dial("tcp", os.Getenv("REDIS_HOST_AND_PORT"))
	if err != nil {
		log.Println(err)
		return err, false
	}

	defer c.Close()

	if _, err := c.Do("AUTH", os.Getenv("REDIS_AUTH")); err != nil {
		log.Println(err)
		return err, false
	}

	c.Do("SELECT", os.Getenv("REDIS_DB"))

	if _, err := redis.Int(c.Do("GET", url)); err == nil {
		return nil, true
	} else if err.Error() == "redigo: nil returned" {
		return nil, false
	} else {
		log.Println(err)
		return err, false
	}

}

func urlToCheck(path string) string {
	return strings.SplitN(path, "/", 4)[3]
}
