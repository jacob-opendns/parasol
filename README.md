# parasol

Parasol responds to requests of the following forms 

	/urlinfo/1/<hostname>
	/urlinfo/1/<hostname>:<port>
	/urlinfo/1/<hostname>:<port>/<path>
	/urlinfo/1/<hostname>:<port>/<path>?<query_string>
		
with a 200.  This response tells the client that parasol knows of malware phoning home to such URLs.  Other requests to paths beginning with /urlinfo/1/ will get an empty 404 response indicating that parasol knows of no suspicious activity involving any such URL.


To install parasol, first install and configure [Go](https://golang.org/), then run

	go get bitbucket.org/jacob-opendns/parasol

then cd to the parasol directory under your GOPATH and run

 	go build

To run parasol, export these environment variables:

- PORT indicating the port on which parasol will listen
- REDIS_AUTH indicating the password for Redis, if any
- REDIS_HOST_AND_PORT indicating the Redis server holding the blacklist 
- REDIS_DB indicating the Redis database holding the blacklist 

and run 

	$GOPATH/bin/parasol


To run parasol's tests, from the parasol directory, run

	go test

To use parasol, run (e.g.)

	curl -IX GET http://parasol_host:port/urlinfo/1/good.com:1337/x?y=z

Assuming your blacklist does not contain an entry for good.com:1337/x?y=z, you will receive an HTTP status code of 404 with an empty body, indicating that parasol has not found a blacklist entry for it. An HTTP status code of 200 indicates that parasol has found a blacklist entry for the supplied url. 

To see a running instance of parasol in action, run 

	curl -IX GET http://parasol-opendns.herokuapp.com/urlinfo/1/badguyz.com:666/foo?bar=baz

to see a 200 response, and 

	curl -IX GET http://parasol-opendns.herokuapp.com/urlinfo/1/goodguyz.com:888/foo?bar=baz

to see a 404.